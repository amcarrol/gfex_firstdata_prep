from datetime import datetime

#run number here
run = '434535'

#data file dumped from Simone's event dumper script
file = open(f"run_{run}.data", "r")
itern = "1"
#file to run through bitwise c simulation
output = open(f"run{run}_processed{itern}.txt", "w")

#extra file containing time (unix time and date time), L1ID, BCID
loutput = open(f"run{run}_other_data{itern}.txt", "w")

#jet tob files
ajtob_out = open(f"run{run}_atobs_dumped.txt","w")
bjtob_out = open(f"run{run}_btobs_dumped.txt","w")
cjtob_out = open(f"run{run}_ctobs_dumped.txt","w")

#global tob files
agtob_out = open(f"run{run}_agtobs_dumped.txt","w")
bgtob_out = open(f"run{run}_bgtobs_dumped.txt","w")
cgtob_out = open(f"run{run}_cgtobs_dumped.txt","w")

#header to match with output of bitwise C simulation
ajtob_out.write("* Placeholder line to match bitwise C simulation \n")
bjtob_out.write("* Placeholder line to match bitwise C simulation \n")
cjtob_out.write("* Placeholder line to match bitwise C simulation \n")
agtob_out.write("* Placeholder line to match bitwise C simulation \n")
bgtob_out.write("* Placeholder line to match bitwise C simulation \n")
cgtob_out.write("* Placeholder line to match bitwise C simulation \n")

alldata = []
pointer = -1
for line in file:
    if line == "\n":
        pointer += 1
        alldata.append([])
    else:
        splitline = line.split(" ")
        for word in splitline:
            if len(word) == 8:
                alldata[pointer].append(word)

file.close()

#Now have the stucture alldata[block number], giving a list of hex words in that event

processeddata = []
#initialize empty tob data to write to files
ajtobdata = []
bjtobdata = []
cjtobdata = []
agtobdata = []
bgtobdata = []
cgtobdata = []

#loop over events
for blockn in range(len(alldata)):
    block = alldata[blockn]
    processeddata.append("")
    #Do processing for each chunk
    ctr = 0
    #loop over words in event
    for wordn in range(len(block)):
        word = block[wordn]

        headerform = word[0] == word[1] and word[6] == word[7] and word[2:6] == "1234" and word[0].isalpha() and \
                     word[1].isalpha() and word[6].isalpha() and word[7].isalpha()
        fiberform = word[1:7] == "140002" or (word[6:] == "bc" and word[1:6] == "14002")
        dzerothreeform = word == "d0000003"
        timeform = word

        if fiberform or headerform:
            if wordn == 0:
                processeddata[blockn] += word + "\n"
            else:
                if ctr != 0:
                    processeddata[blockn] += "\n" + "\n" + word + "\n"
                else:
                    processeddata[blockn] += "\n" + word + "\n"
            ctr = 0
        elif dzerothreeform:
            processeddata[blockn] += word + "\n"
            ctr = 0
        else:
            ctr += 1
            if ctr == 7:
                processeddata[blockn] += word + "\n"
                ctr = 0
            else:
                processeddata[blockn] += word + " "

processeddata2 = []
outrunn = 0
# for blockn in range(10):
for blockn in range(len(processeddata)):
    block = processeddata[blockn]
    blockdata = block.split("\n")
    fibers = ""
    startslice = -1
    endslice = -1
    startslicemeta = -1
    endslicemeta = -1
    runn = ""
    bcid = ""
    lid = ""
    datetimereadable = ""
    utime = "0"
    metaheader = ""
    metaheader2 = ""
    metaheader3 = "FI_INP 100"
    for linen in range(len(blockdata)):
        line = blockdata[linen]
        endish = linen + 3 >= len(blockdata)
        metafilter = len(line) == 8 and line == "aa1234aa"
        if not endish:
            fiberfilter = len(line) == 8 and line[6:] == "bc" and line[1:6] == "14002" and \
                          len(blockdata[linen + 2]) != 0 and len(blockdata[linen + 3]) != 0
        else:
            fiberfilter = False

        if metafilter:
            startslicemeta = linen + 1
            endslicemeta = linen + 3

        if startslicemeta <= linen <= endslicemeta:
            metadatalist = line.split(" ")
            toprint = ""
            for wordn in range(len(metadatalist)):
                word = metadatalist[wordn]
                if endslicemeta - linen == 2:
                    if wordn == 0:
                        toprint += "Event Size: "
                    elif wordn == 6:
                        utime = int(word, 16)
                        datetimereadable = datetime.utcfromtimestamp(utime + 7200).strftime('%Y-%m-%d %H:%M:%S')
                        utime = str(utime)
                if endslicemeta - linen == 1:
                    if wordn == 4:
                        toprint += "Run Number: "
                        metaheader += f"RUN {str(int(word, 16)).upper().lstrip(str(0))} "
                        runn = str(int(word, 16)).upper().lstrip("0")
                        outrunn = runn
                    elif wordn == 6:
                        lid = str(int(word, 16)).upper()
                if endslicemeta - linen == 0:
                    if wordn == 0:
                        toprint += "BCID: "
                        metaheader2 += f"BCID {word[4:].upper()}"
                        bcid = word[4:].upper()
                toprint += word + " "
            # output.write(toprint)
            # output.write()

        if fiberfilter:
            fibers += line[0]
            startslice = linen + 1
            endslice = startslice + 99

            if line[0] == "b" and len(fibers) == 1:
                # output.write("b len 1")
                output.write(metaheader + lid + " A " + utime + "\n")
                output.write(metaheader2 + "\n")
                output.write(metaheader3 + "\n")

                for i in range(100):
                    output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")
            elif line[0] == "c" and len(fibers) == 1:
                # output.write("c len 1")
                output.write(metaheader + lid + " A " + utime + "\n")
                output.write(metaheader2 + "\n")
                output.write(metaheader3 + "\n")

                for i in range(100):
                    output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

                output.write(metaheader + lid + " B " + utime + "\n")
                output.write(metaheader2 + "\n")
                output.write(metaheader3 + "\n")

                for i in range(100):
                    output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")
            elif line[0] == "c" and len(fibers) == 2 and fibers[0] == "a":
                # output.write("ca len 2")
                output.write(metaheader + lid + " B " + utime + "\n")
                output.write(metaheader2 + "\n")
                output.write(metaheader3 + "\n")

                for i in range(100):
                    output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")
            elif line[0] == "c" and len(fibers) == 2 and fibers[0] == "b":
                # output.write("cb len 2")
                output.write(metaheader + lid + " A " + utime + "\n")
                output.write(metaheader2 + "\n")
                output.write(metaheader3 + "\n")

                for i in range(100):
                    output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

            if line[0] == "c":
                output.write(metaheader + lid + " " + line[0].upper() + line[0].upper() + " " + utime + "\n")
            else:
                output.write(metaheader + lid + " " + line[0].upper() + " " + utime + "\n")
            output.write(metaheader2 + "\n")
            output.write(metaheader3 + "\n")

        if startslice <= linen <= endslice:
            output.write(line + "\n")

    if len(fibers) == 0:
        # output.write("null")
        output.write(metaheader + lid + " A " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

        output.write(metaheader + lid + " B " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

        output.write(metaheader + lid + " CC " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

    elif len(fibers) == 1 and fibers[0] == "a":
        # output.write("a len 1")
        output.write(metaheader + lid + " B " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

        output.write(metaheader + lid + " CC " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

    elif len(fibers) == 1 and fibers[0] == "b":
        # output.write("b len 1 out")
        output.write(metaheader + lid + " CC " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

    elif len(fibers) == 2 and fibers[0] == "a" and fibers[1] == "b":
        # output.write("ab len 2")
        output.write(metaheader + lid + " CC " + utime + "\n")
        output.write(metaheader2 + "\n")
        output.write(metaheader3 + "\n")

        for i in range(100):
            output.write("00000000 00000000 00000000 00000000 00000000 00000000 00000000" + "\n")

    output.write(f"EOE {lid} {utime}" + "\n")
    loutput.write(f"RUN {runn} {blockn}" + "\n")
    loutput.write(f"BCID {bcid}" + "\n")
    loutput.write(f"L1ID {lid}" + "\n")
    loutput.write(f"Unix time: {utime}, Datetime (UTC+2): {datetimereadable}" + "\n")  # Remove " + 7200" to put back to UTC. 3600 for every hour to account for timezones

output.write(f"EOF {outrunn}")

output.close()
loutput.close()
ajtob_out.close()
bjtob_out.close()
cjtob_out.close()
agtob_out.close()
bgtob_out.close()
cgtob_out.close()

