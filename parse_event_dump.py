#script to take a file of dumped gfex events and compare the header BCID to the BCID in the TOB trailer
#parts based on gFEX_dumped_dataprep.py originially written as gFEX_analysis.py by Bayley Burke

#libraries
from datetime import datetime
import numpy as np
from ROOT import TCanvas, TH1F

#run is run number, file is the input data file, adding conditionals for different modes (bcid check, for e.g.) these should be the only lines that require manual change when running this script
run = '434535'
file = open(f"run_434535.data", "r")
bcid_mode = 0

#currently 3 tob slices (used for number of expected TOBs per fpga in trailer)
tobslice = 3

#fpga and tob arrays for easier printing
fpga_arr = ['A','B','C']
tob_arr = ['A','B','C','1','2','3']

#output files
if bcid_mode == 1:
    #BCID comparison
    bcid_out = open(f"run{run}_bcidcheck.txt", "w")

#List of missing signifiers
miss = open(f"run{run}_missing_data.txt", "w")

#Jet and MET TOB output for comparison to simulation
ajtob_out = open(f"run{run}_dump_ajtobs.txt", "w")
bjtob_out = open(f"run{run}_dump_bjtobs.txt", "w")
cjtob_out = open(f"run{run}_dump_cjtobs.txt", "w")
agtob_out = open(f"run{run}_dump_agtobs.txt", "w")
bgtob_out = open(f"run{run}_dump_bgtobs.txt", "w")
cgtob_out = open(f"run{run}_dump_cgtobs.txt", "w")

#TODO: shared_quads.txt file to run through bitwise simulation
sim_prep = open(f"file{run}.shared_quads.txt","w")

#preambles

if bcid_mode == 1:
    bcid_out.write(f"Run {run} BCID Comparisons\n")

miss.write(f"Run {run} Missing Data List\n")
ajtob_out.write(f"*Run {run} FPGA A Jet TOBs\n")
bjtob_out.write(f"*Run {run} FPGA B Jet TOBs\n")
cjtob_out.write(f"*Run {run} FPGA C Jet TOBs\n")
agtob_out.write(f"*Run {run} FPGA A MET TOBs\n")
bgtob_out.write(f"*Run {run} FPGA B MET TOBs\n")
cgtob_out.write(f"*Run {run} FPGA C MET TOBs\n")


#parse the data into an array for indexing -- taken from gFEX_dumped_dataprep.py
alldata = []
pointer = -1
for line in file:
    if line == "\n":
        pointer += 1
        alldata.append([])
    else:
        splitline = line.split(" ")
        for word in splitline:
            #make sure we aren't getting weird fragments
            if len(word) == 8:
                alldata[pointer].append(word)

file.close()

nevent = len(alldata)

#now alldata contains every event in the data file, so alldata[0] is the first event, etc

#create None arrays to pass values into
L1ID = [None] * nevent 
Unix_time = [None] * nevent
headBCID = [None] * nevent
tobBCID = [None] * nevent

ahead = [None] * nevent
dhead1 = [None] * nevent
ehead1 = [None] * nevent
dhead2 = [None] * nevent
ehead2 = [None] * nevent

afiber = [None] * nevent
bfiber = [None] * nevent
cfiber = [None] * nevent

afiber_stat = [None] * nevent
bfiber_stat = [None] * nevent
cfiber_stat = [None] * nevent

ajtob = [None] * nevent
bjtob = [None] * nevent
cjtob = [None] * nevent
agtob = [None] * nevent
bgtob = [None] * nevent
cgtob = [None] * nevent

ajtob_stat = [None] * nevent
bjtob_stat = [None] * nevent
cjtob_stat = [None] * nevent
agtob_stat = [None] * nevent
bgtob_stat = [None] * nevent
cgtob_stat = [None] * nevent

#create zero masks for missing data
zero_stat = np.repeat('00000000', 3)
zero_fiber = np.repeat('00000000', 700)
zero_tob = np.repeat('00000000', 14*tobslice)
zero_head23 = np.repeat('00000000', 23)
zero_head9 = np.repeat('00000000', 9)

#missing counts for tracking which events had missing pieces of data
miss_ahead = 0
miss_ehead1 = 0
miss_ehead2 = 0
miss_dhead1 = 0
miss_dhead2 = 0
miss_afib = 0
miss_bfib = 0
miss_cfib = 0
miss_afib_stat = 0
miss_bfib_stat = 0
miss_cfib_stat = 0
miss_ajtob = 0
miss_bjtob = 0
miss_cjtob = 0
miss_agtob = 0
miss_bgtob = 0
miss_cgtob = 0
miss_ajtob_stat = 0
miss_bjtob_stat = 0
miss_cjtob_stat = 0
miss_agtob_stat = 0
miss_bgtob_stat = 0
miss_cgtob_stat = 0

#arrays to store missing data indices
miss_ahead_arr = []
miss_ehead1_arr = []
miss_ehead2_arr = []
miss_dhead1_arr = []
miss_dhead2_arr = []
miss_afib_arr = []
miss_bfib_arr = []
miss_cfib_arr = []
miss_afib_stat_arr = []
miss_bfib_stat_arr = []
miss_cfib_stat_arr = []
miss_ajtob_arr = []
miss_bjtob_arr = []
miss_cjtob_arr = []
miss_agtob_arr = []
miss_bgtob_arr = []
miss_cgtob_arr = []
miss_ajtob_stat_arr = []
miss_bjtob_stat_arr = []
miss_cjtob_stat_arr = []
miss_agtob_stat_arr = []
miss_bgtob_stat_arr = []
miss_cgtob_stat_arr = []

#loop through events
for i in range(0,nevent):
    event = alldata[i]

    #flags for appending the first or second e and d headers,or for correctly parsing status parts of fibers/tobs
    dheadflag = 0
    eheadflag = 0
    afibflag = 0
    bfibflag = 0
    cfibflag = 0
    ajtobflag = 0
    bjtobflag = 0
    cjtobflag = 0
    agtobflag = 0
    bgtobflag = 0
    cgtobflag = 0

    #loop through words in event
    for j in range(0,len(event)):
        word = event[j]

        #define bools for different signifiers

        #header start is of the form xx1234xx, x=a,d,e (maybe others??)
        isheader = (word[2:6] == "1234" and word[0] == word[1] and word[6] == word[7] and word[0].isalpha() and word[7].isalpha())

        #fiber start is given by x14002bc, x=a,b,c
        isfiber = (word[1:] == "14002bc")

        #tob trailer start is of the form x140002a, x=a,b,c,1,2,3
        istob = (word[1:] == "140002a")

        #also want the d status words, not used right now but could be useful
        isdstatus = (word == "d0000002" or word == "d0000003")

        #signifier for zero word, not used right now but could be useful
        iszero = (word =="00000000")

        #first we will deal with headers, there are three in the beginning and two near the end of a packet
        if (isheader):
            #length of header (for indexing)
            headlen = 0
            #if the header starts with a or d, the word two after the start is the length of the header
            if (word[0] == 'a' or word[0] == 'd'):
                headlen = int(event[j+2], 16)
            #if header starts with e, the word after is the length... for some reason
            elif (word[0] == 'e'):
                headlen = int(event[j+1], 16)

            #properly store the headers
            if (word[0] == 'a'):
                ahead[i] = event[j: j+headlen]
            if (word[0] == 'd'):
                if(dheadflag == 0):
                    dhead1[i] = event[j: j+headlen]
                    dheadflag = 1
                else:
                    dhead2[i] = event[j: j+headlen]
            if (word[0] == 'e'):
                if(eheadflag == 0):
                    ehead1[i] = event[j: j+headlen]
                    eheadflag = 1
                else:
                    ehead2[i] = event[j: j+headlen]


        if (isfiber):
            #fiber signifier will be followed by 7*100 = 700 words of fiber data, or is the start of the status with 3 words following
            if (word[0] == 'a'):
                if(afibflag == 0):
                    afiber[i] = event[j+1:j+701]
                    afibflag = 1
                else:
                    afiber_stat[i] = event[j+1:j+4]
            if (word[0] == 'b'):
                if(bfibflag == 0):
                    bfiber[i] = event[j+1:j+701]
                    bfibflag = 1
                else:
                    bfiber_stat[i] = event[j+1:j+4]
            if (word[0] == 'c'):
                if(cfibflag == 0):
                    cfiber[i] = event[j+1:j+701]
                    cfibflag = 1
                else:
                    cfiber_stat[i] = event[j+1:j+4]

        if (istob):
            #similarly to fiber signifier, tob signifier will be followed by 14*tobslice words if the tobs, or 3 words if the status
            if (word[0] == 'a'):
                if(ajtobflag == 0):
                    ajtob[i] = event[j+1: j+1+14*tobslice]
                    ajtobflag = 1
                else:
                    ajtob_stat[i] = event[j+1:j+4]
            if (word[0] == 'b'):
                if(bjtobflag == 0):
                    bjtob[i] = event[j+1: j+1+14*tobslice]
                    bjtobflag = 1
                else:
                    bjtob_stat[i] = event[j+1:j+4]
            if (word[0] == 'c'):
                if(cjtobflag == 0):
                    cjtob[i] = event[j+1: j+1+14*tobslice]
                    cjtobflag = 1
                else:
                    cjtob_stat[i] = event[j+1:j+4]
            if (word[0] == '1'):
                if(agtobflag == 0):
                    agtob[i] = event[j+1: j+1+14*tobslice]
                    agtobflag = 1
                else:
                    agtob_stat[i] = event[j+1:j+4]
            if (word[0] == '2'):
                if(bgtobflag == 0):
                    bgtob[i] = event[j+1: j+1+14*tobslice]
                    bgtobflag = 1
                else:
                    bgtob_stat[i] = event[j+1:j+4]            
            if (word[0] == '3'):
                if(cgtobflag == 0):
                    cgtob[i] = event[j+1: j+1+14*tobslice]
                    cgtobflag = 1
                else:
                    cgtob_stat[i] = event[j+1:j+4]

    #before moving on to the next event, fill any missing arrays with a mask of zeroes, keep track of indices
    if (ahead[i] is None):
        ahead[i] = (zero_head23)
        miss_ahead += 1
        miss_ahead_arr.append(i)
    if (ehead1[i] is None):
        ehead1[i] = (zero_head9)
        miss_ehead1 += 1
        miss_ehead1_arr.append(i)
    if (ehead2[i] is None):
        ehead2[i] = (zero_head9)
        miss_ehead2 += 1
        miss_ehead2_arr.append(i)
    if (dhead1[i] is None):
        dhead1[i] = (zero_head9)
        miss_dhead1 += 1
        miss_dhead1_arr.append(i)
    if (dhead2[i] is None):
        dhead2[i] = (zero_head9)
        miss_dhead2 += 1
        miss_dhead2_arr.append(i)
    if (afiber[i] is None):
        afiber[i] = (zero_fiber)
        miss_afib += 1
        miss_afib_arr.append(i)
    if (bfiber[i] is None):
        bfiber[i] = (zero_fiber)
        miss_bfib += 1
        miss_bfib_arr.append(i)
    if (cfiber[i] is None):
        cfiber[i] = (zero_fiber)
        miss_cfib += 1
        miss_cfib_arr.append(i)
    if (afiber_stat[i] is None):
        afiber_stat[i] = (zero_stat)
        miss_afib_stat += 1
        miss_afib_stat_arr.append(i)
    if (bfiber_stat[i] is None):
        bfiber_stat[i] = (zero_stat)
        miss_bfib_stat += 1
        miss_bfib_stat_arr.append(i)
    if (cfiber_stat[i] is None):
        cfiber_stat[i] = (zero_stat)
        miss_cfib_stat += 1
        miss_cfib_stat_arr.append(i)
    if (ajtob[i] is None):
        ajtob[i] = (zero_tob)
        miss_ajtob += 1
        miss_ajtob_arr.append(i)
    if (bjtob[i] is None):
        bjtob[i] = (zero_tob)
        miss_bjtob += 1
        miss_bjtob_arr.append(i)
    if (cjtob[i] is None):
        cjtob[i] = (zero_tob)
        miss_cjtob += 1
        miss_cjtob_arr.append(i)
    if (agtob[i] is None):
        agtob[i] = (zero_tob)
        miss_agtob += 1
        miss_agtob_arr.append(i)
    if (bgtob[i] is None):
        bgtob[i] = (zero_tob)
        miss_bgtob += 1
        miss_bgtob_arr.append(i)
    if (cgtob[i] is None):
        cgtob[i] = (zero_tob)
        miss_cgtob += 1
        miss_cgtob_arr.append(i)
    if (ajtob_stat[i] is None):
        ajtob_stat[i] = (zero_stat)
        miss_ajtob_stat += 1
        miss_ajtob_stat_arr.append(i)
    if (bjtob_stat[i] is None):
        bjtob_stat[i] = (zero_stat)
        miss_bjtob_stat += 1
        miss_bjtob_stat_arr.append(i)
    if (cjtob_stat[i] is None):
        cjtob_stat[i] = (zero_stat)
        miss_cjtob_stat += 1
        miss_cjtob_stat_arr.append(i)
    if (agtob_stat[i] is None):
        agtob_stat[i] = (zero_stat)
        miss_agtob_stat += 1
        miss_agtob_stat_arr.append(i)
    if (bgtob_stat[i] is None):
        bgtob_stat[i] = (zero_stat)
        miss_bgtob_stat += 1
        miss_bgtob_stat_arr.append(i)
    if (cgtob_stat[i] is None):
        cgtob_stat[i] = (zero_stat)
        miss_cgtob_stat += 1
        miss_cgtob_stat_arr.append(i)
    
    #Now we have arrays for all the parsed data that are index by event in the file, so we put in the info we want
    L1ID[i] = ahead[i][14]
    Unix_time[i] = ahead[i][7]
    headBCID[i] = ahead[i][15]
    tobBCID[i] = ajtob[i][6]

if bcid_mode == 1:
    #separate out the writing to file for convenience
    count = 0
    bcid_diff = []
    for i in range(0,nevent):
        if (headBCID[i][7] != tobBCID[i][5]):
            count += 1
            bcid_out.write(f"Run {run} L1ID {int(L1ID[i],16)}\n")
            bcid_out.write(f"Header BCID  : {headBCID[i]}\n")
            bcid_out.write(f"Jet TOB BCID : {tobBCID[i]}\n")
            bcid_out.write("Hex\n")
            bcid_out.write(f"Header       : {headBCID[i][7]}\n")
            bcid_out.write(f"TOB          : {tobBCID[i][5]}\n")
            bcid_out.write(f"Binary\n")
            bcid_out.write(f"Header       : {bin(int(headBCID[i][7],16)).zfill(10)}\n")
            bcid_out.write(f"TOB          : {bin(int(tobBCID[i][5],16)).zfill(10)}\n")
            diff = int(headBCID[i][7],16) - int(tobBCID[i][5],16)
            bcid_diff.append(diff)
            bcid_out.write(f"Difference   : {diff}\n \n")  

    bcid_out.write(f"Total number of mismatches in file: {count}/{nevent} events\n")

    #make histogram of differences
    #set parameters
    c1 = TCanvas('c1', 'Example', 200, 10, 700, 500)
    hbcid_diff = TH1F('hbcid_diff', f"Run {run} Mismatched BCID Counts", 100, -100, 200)
    #fill histogram
    for i in range(0,len(bcid_diff)):
        hbcid_diff.Fill(bcid_diff[i])

    #label axes
    hbcid_diff.GetYaxis().SetTitle("Counts")
    hbcid_diff.GetXaxis().SetTitle("Header BCID - TOB BCID")

    #Draw and save histogram to file    
    hbcid_diff.Draw()
    c1.Update()
    c1.Print(f"Run{run}_BCIDcheck_hist.eps")

#loop to find missing data events and write them to file
miss_total = miss_ahead + miss_ehead1 + miss_ehead2 + miss_dhead1 + miss_dhead2 + miss_afib + miss_bfib         + miss_cfib + miss_afib_stat + miss_bfib_stat + miss_cfib_stat + miss_ajtob + miss_bjtob + miss_cjtob + miss_agtob + miss_bgtob + miss_cgtob + miss_ajtob_stat + miss_bjtob_stat + miss_cjtob_stat + miss_agtob_stat + miss_bgtob_stat + miss_cgtob_stat
for i in range(0, nevent):
    if i in miss_ahead_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : aa1234aa header\n")
    if i in miss_ehead1_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : first ee1234ee header\n")
    if i in miss_ehead1_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : second ee1234ee header\n")
    if i in miss_dhead1_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : first dd1234dd header\n")
    if i in miss_dhead2_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : second dd1234dd header\n")
    if i in miss_afib_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A Fiber Data\n")
    if i in miss_bfib_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B Fiber Data\n")
    if i in miss_cfib_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C Fiber Data\n")
    if i in miss_afib_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A Fiber Status\n")
    if i in miss_bfib_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B Fiber Status\n")
    if i in miss_cfib_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C Fiber Status\n")
    if i in miss_ajtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A Jet TOB\n")
    if i in miss_bjtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B Jet TOB\n")
    if i in miss_cjtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C Jet TOB\n")
    if i in miss_agtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A MET TOB\n")
    if i in miss_bgtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B MET TOB\n")
    if i in miss_cgtob_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C MET TOB\n")
    if i in miss_ajtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A Jet TOB status\n")
    if i in miss_bjtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B Jet TOB status\n")
    if i in miss_cjtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C Jet TOB status\n")
    if i in miss_agtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA A MET TOB status\n")
    if i in miss_bgtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA B MET TOB status\n")
    if i in miss_cgtob_stat_arr:
        miss.write(f"Missing Data from event {i} in data file\n")
        miss.write(f"BCID : {headBCID[i]}   L1ID : {L1ID[i]}   Missing Signifier : pFPGA C MET TOB status\n")

miss.write(f"Total Number of Missed Data : {miss_total}")

#Write out the dumped TOBs for comparison to TOBs generated from simulation
for i in range(0,nevent):
    #Jet TOB FPGA A
    ajtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    ajtob_out.write(f"ATOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        ajtob_out.write(f"{j} {ajtob[i][j]}\n")
    ajtob_out.write(f"ATOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        ajtob_out.write(f"{j-7} {ajtob[i][j]}\n")

    #Jet TOB FPGA b
    bjtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    bjtob_out.write(f"BTOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        bjtob_out.write(f"{j} {bjtob[i][j]}\n")
    bjtob_out.write(f"BTOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        bjtob_out.write(f"{j-7} {bjtob[i][j]}\n")

    #Jet TOB FPGA C
    cjtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    cjtob_out.write(f"CTOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        cjtob_out.write(f"{j} {cjtob[i][j]}\n")
    cjtob_out.write(f"CTOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        cjtob_out.write(f"{j-7} {cjtob[i][j]}\n")

    #MET TOB FPGA A
    agtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    agtob_out.write(f"ATOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        agtob_out.write(f"{j} {agtob[i][j]}\n")
    agtob_out.write(f"ATOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        agtob_out.write(f"{j-7} {agtob[i][j]}\n")

    #MET TOB FPGA B
    bgtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    bgtob_out.write(f"BTOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        bgtob_out.write(f"{j} {bgtob[i][j]}\n")
    bgtob_out.write(f"BTOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        bgtob_out.write(f"{j-7} {bgtob[i][j]}\n")

    #MET TOB FPGA C
    cgtob_out.write(f"* run {run} event {int(L1ID[i],16)}\n")
    cgtob_out.write(f"CTOB1_dat {headBCID[i]}\n")
    for j in range(0,7):
        cgtob_out.write(f"{j} {cgtob[i][j]}\n")
    cgtob_out.write(f"CTOB2_dat {headBCID[i]}\n")
    for j in range(7,14):
        cgtob_out.write(f"{j-7} {cgtob[i][j]}\n")

#create shared_quads data file to run through standalone C simulation
for i in range(0, nevent):
    for j in range(0,len(fpga_arr)):
        if j == 0:
            fib_data = afiber[i]
        elif j == 1:
            fib_data = bfiber[i]
        else:
            fib_data = cfiber[i]
        #header for each event before fiber data
        sim_prep.write(f"RUN {run} {int(L1ID[i],16)} {fpga_arr[j]} {int(Unix_time[i],16)}\n")
        sim_prep.write(f"BCID {headBCID[i][4:].upper()}\n")
        sim_prep.write("FI_INP 100\n")

        #fiber data
        #check that everything is there
        if len(fib_data) == 700:
            #100 lines of 7 words each
            lines = [None] * 100
            for k in range(0,len(lines)):
                lines[k] = fib_data[7*k] + " " + fib_data[7*k+1] + " "+ fib_data[7*k+2] + " " + fib_data[7*k+3]+ " " + fib_data[7*k+4]+ " " + fib_data[7*k+5]+ " " + fib_data[7*k+6] + "\n"
                sim_prep.write(lines[k])  
        else:
            sim_prep.write(f"Error: Incorrect Fiber Data Packet Size\n")
    sim_prep.write(f"EOE {int(L1ID[i],16)} {int(Unix_time[i],16)}\n")
sim_prep.write("EOF {run}")


if bcid_mode == 1:
    bcid_out.close()
miss.close()
ajtob_out.close()
bjtob_out.close()
cjtob_out.close()
agtob_out.close()
bgtob_out.close()
cgtob_out.close()
sim_prep.close()
